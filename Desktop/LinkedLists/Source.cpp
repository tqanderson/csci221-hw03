#include <iostream>
using namespace std;

struct node {
	int data;
	struct node* next;
};

void Push(struct node** headRef, int dataValue) {
	struct node* newNode = new struct node;
	newNode->data = dataValue;
	newNode->next = *headRef;
	*headRef = newNode;
}

void Print(struct node* head) {
	struct node* current = head;

	while (current != NULL) {
		cout << current->data << endl;
		current = current->next;
	}
}

int Length(struct node* head) {
	int count = 0;
	struct node* current = head;

	while (current != NULL) {
		//cout << current->data << endl;
		count++;
		current = current->next;
	}
	return count;
}

void AddToEnd1(struct node** headRef, int dataValue) {
	struct node* newNode = new struct node;
	newNode->data = dataValue;
	if ((*headRef) != NULL) {
		struct node* current = *headRef;
		while (current->next != NULL) {
			current = current->next;
		}
		newNode->next = current->next;
		current->next = newNode;
	}
	else { // The list is empty!
		newNode->next = NULL;
		*headRef = newNode;
	}

}

void AddToEnd2(struct node** headRef, int dataValue) {
	struct node* current = *headRef;
	if (current == NULL) {
		Push(headRef, dataValue);
	}
	else {
		while (current->next != NULL) {
			current = current->next;
		}
		Push(&(current->next), dataValue);
	}

}

void RemoveNode(struct node** headRef, int dataValue) {
	struct node* current = *headRef;
	struct node* prev = NULL;

	while (current != NULL) {

		if (current->data == dataValue) { //check if the node is found
			if (current == *headRef) {
				*headRef = current->next;
			}
			else {
				prev->next = current->next;
				delete current;
				return;
			}
		}
		prev = current;
		current = current->next;
	}
}

struct node* FindKthLast(struct node* head, int k) {
	int count = 0;
	int iterate = 0;
	struct node* current = head;

	while (current != NULL) {
		count++;
		current = current->next;
	}
	iterate = count - k;
	if (iterate < 0) {
		cout << "k is bigger than length of the list\n";
		return NULL;
	}

	current = head;
	while (iterate > 0) {

		iterate--;
		current = current->next;
	}
	return current;

}


struct node* FindKthLastPointers(struct node* head, int k) {
	struct node* current = head;
	struct node* runner = head;

	while ((runner != NULL) && (k > 0)) {
		runner = runner->next;
		k--;
		if ((runner == NULL) && (k > 0)) {
			cout << "k is bigger than length of the list\n";
			return NULL;
		}
		runner = runner->next;
		current = current->next;
	}

	while (runner != NULL) {
		runner = runner->next;
		current = current->next;
	}

	return current;
}

void Reverse(struct node** headRef) {
	struct node* newlist = NULL;
	struct node* current = *headRef;
	struct node* next;

	while (current != NULL) {
		next = current->next;
		current->next = newlist;
		newlist = current;
		current = next;

	}
	*headRef = newlist;
}


void InsertSorted(struct node** headRef, int dataValue) {
	struct node* newNode = new struct node;
	newNode->data = dataValue;

	struct node* current = *headRef;

	if (((*headRef) == NULL) || newNode->data <= (*headRef)->data) {  //for the first and second (if it is smaller than the first one)
		newNode->next = current;
		*headRef = newNode;

	}
	else {

		while ((current->next != NULL) && ((current->next)->data <= newNode->data)) {
			current = current->next;
		}
		newNode->next = current->next;
		current->next = newNode;
	}
}

/*void listSort(struct node* head) {
	//Node current will point to head  
	struct node* current = head, * index = NULL;
	int temp;

	if (head == NULL) {
		return;
	}
	else {
		while (current != NULL) {
			//Node index will point to node next to current  
			index = current->next;

			while (index != NULL) {
				//If current node's data is greater than index's node data, swap the data between them  
				if (current->data > index->data) {
					temp = current->data;
					current->data = index->data;
					index->data = temp;
				}
				index = index->next;
			}
			current = current->next;
			
			//cout << current->data << endl;
		}
	}
}*/

void removeDuplicateSorted(struct node* head) {
	struct node* current = head, * index = NULL;
	int temp;

	if (head == NULL) {
		return;
	}
	else {
		while (current != NULL) { 
			index = current->next;

			while (index != NULL) {  
				if (current->data > index->data) {
					temp = current->data;
					current->data = index->data;
					index->data = temp;
				}
				index = index->next;
			}
			current = current->next;
		}
		current = head;
		while (current != NULL && current->next != NULL) {
			index = current->next;
			if (current->data == index->data) {
				RemoveNode(&head, current->data);
			}
			current = index;
		}

	}
}

void removeDuplicateUnsortedV1(struct node* head) {
	struct node* current = head;
	struct node* runner = head;
	//int j = 0;
	//int k = 0;
	int deletion = 0;

	 while (current != NULL && current->next != NULL) {
		//j++;
		//cout << j << endl;
		//k = 0;
		deletion = 0;
		runner = current->next;
			while (runner != NULL) {
				//k++;
				//cout << " " <<  k << endl;
				if (current->data == runner->data) {
					//cout << "found match " << runner->data << endl;
					RemoveNode(&current, runner->data);
					deletion = 1;
					break;
				}
				runner = runner->next;
			}
			if (deletion == 0) {
				current = current->next;
			}
	}
}

void removeDuplicateUnsortedV2(struct node* head) {
	struct node* current   = head;
	struct node* runner    = NULL;
	struct node* duplicate = NULL;
	//int k = 0;
	//int j = 0;
	int del = 0;

	while (current != NULL && current->next != NULL) {
		//j++;
		//cout << current->data << endl;
		//k = 0;
		del = 0;
		runner = current;
		while (runner->next != NULL) {
			//k++;
			//cout << "  " << runner->data << endl;
			if (current->data == runner->next->data) {
				//cout << "found match (" << runner->data << ")" << endl;

				duplicate = runner->next;
				runner->next = runner->next->next;
				delete duplicate;
			} else {
				runner = runner->next;
			}
		}
		current = current->next;
	}
}


int main() {
	struct node* head = NULL;

	AddToEnd1(&head, 10);
	AddToEnd1(&head, 80);
	AddToEnd1(&head, 10);
	AddToEnd1(&head, 90);
	AddToEnd1(&head, 30);
	AddToEnd1(&head, 80);
	AddToEnd1(&head, 70);
	AddToEnd1(&head, 30);
	AddToEnd1(&head, 20);
	AddToEnd1(&head, 100);
	AddToEnd1(&head, 200);

	cout << endl;

	//RemoveNode(&head, 30);
	//Print(head);
	//cout << endl;

	//struct node* temp = FindKthLastPointers(head, 2);
	//if(temp != NULL)
	//cout << "2nd from last: " <<
	//	temp->data <<endl;

	
	//cout << "Length is : " << Length(head); //length of link list
	//cout << endl;
	//AddToEnd1(&head,50);
	//Print(head);
	//cout << endl;
	//cout << "Length is : " << Length(head); //length of link list
	//cout << endl; 
	//cout << "about to add 90" << endl;
	AddToEnd2(&head,90); //adds 90
	cout << "List of values: " << endl;
		Print(head);
	cout << endl;
	//cout << "length after 90" << endl;
	cout << "Length is : " << Length(head) << endl;
	cout << endl;
	
	
	//Solutions to problems\\

	cout << "This is the sorted list without duplicates: ";
		removeDuplicateSorted(head);
	
	//cout << "This is the unsorted list without duplicates (V1): ";
		//removeDuplicateUnsortedV1(head);
	
	//cout << "This is the unsorted list without duplicates (V2): ";
		//removeDuplicateUnsortedV2(head);

	Print(head);
	
	return 0;
}